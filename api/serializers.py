from rest_framework import serializers
from .models import Student


# Validators
def starts_with_r(value):
    if value[0].lower() != 'r':
        raise serializers.ValidationError("Name must start with R")


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        # fields = "__all__"
        fields = ['name', 'roll', 'city']

    name = serializers.CharField(validators=[starts_with_r])

    # Field level Validation (used for single validation)
    def validate_roll(self, value):
        if value >= 200:
            raise serializers.ValidationError("Seats are full")  # Raising the error
        return value

    # Object level Validation (used for multiple validations)
    def validate(self, data):
        name = data.get('name')
        city = data.get('city')

        if name.lower() == "sachnaam bedi" and city.lower() == "ahmedabad":
            raise serializers.ValidationError("City must be Amritsar")
        return data
